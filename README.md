# [Project 3 - Mobile app testing](https://hackmd.io/@xlYUTygoRkyuQQlwXuWDWQ/rkxJMH_U8)

Student ID: r08921a26

Make sure the app is opened and at the homepage when the test starts.

<hr />

## Get Started

* `git clone https://gitlab.com/r07943154/st2020_project3.git`
* `cd st2020_project3`
* Complete TODO in <span>test.py</span> 
* Create and push to your **public** Gitlab repo.

## TODO

### 1. [Content] Side Bar Text

![](https://i.imgur.com/OBKYDe1.png)

### 2. [Screenshot] Side Bar Text

### 3. [Context] Categories 

![](https://i.imgur.com/RjoQ5Vw.png)

### 4. [Screenshot] Categories 

### 5. [Context] Categories page

![](https://i.imgur.com/nHCxkOs.png)

### 6. [Screenshot] Categories page


### 7. [Behavior] Search item "switch"


* `adb shell input text "mytext"`
* `adb shell input keyevent "KEYCODE_ENTER"`

![](https://i.imgur.com/1nkQwCC.png)

### 8. [Behavior] Follow an item and it should be add to the list

![](https://i.imgur.com/tDl18TC.png)

### 9. [Behavior] Navigate tto the detail of item

![](https://i.imgur.com/xe5NVGz.png)



### 10. [Screenshot] Disconnetion Screen

![](https://i.imgur.com/5Z6W6kT.png)
