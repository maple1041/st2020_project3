# -*- coding: utf-8 -*
import os
from time import sleep
# [Content] XML Footer Text


def test_hasHomePageNode():
    os.system(
        'adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen


def test_tapSidebar():
    os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text


def test_Sidebar():
    sleep(5)
    os.system(
        'adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('查看商品分類') != -1
    assert xmlString.find('查訂單/退訂退款') != -1
    assert xmlString.find('追蹤/買過/看過清單') != -1
    assert xmlString.find('智慧標籤') != -1
    assert xmlString.find('PChome 旅遊') != -1
    assert xmlString.find('線上手機回收') != -1
    assert xmlString.find('給24h購物APP評分') != -1
    f.close()

    # 2. [Screenshot] Side Bar Text


def test_screenShotSidebar():
    os.system(
        'adb shell screencap /sdcard/screen.png && adb pull /sdcard/screen.png sidebar.png')
    # 3. [Context] Categories


def test_Categories():
    os.system('adb shell input keyevent 4')
    sleep(2)
    for i in range(3):
        os.system('adb shell input swipe 500 1000 1000 200')
    sleep(2)
    os.system('adb shell input tap 1020 295')
    sleep(2)
    os.system(
        'adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('精選') != -1
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1
    f.close()

    # 4. [Screenshot] Categories


def test_screenCate():
    os.system(
        'adb shell screencap /sdcard/screen.png && adb pull /sdcard/screen.png categories.png')
    # 5. [Context] Categories page


def test_catePage():
    os.system('adb shell input tap 1025 280')
    sleep(5)
    os.system('adb shell input tap 335 1718')
    sleep(5)
    os.system(
        'adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1
    f.close()

    # 6. [Screenshot] Categories page


def test_catePagescreen():
    os.system(
        'adb shell screencap /sdcard/screen.png && adb pull /sdcard/screen.png catepage.png')
    # 7. [Behavior] Search item “switch”


def test_search():
    os.system('adb shell input tap 120 1714')
    sleep(2)
    os.system('adb shell input tap 624 124')
    sleep(5)
    os.system(
        'adb shell input text "switch"')
    sleep(3)
    os.system('adb shell input keyevent "KEYCODE_ENTER"')
    # 8. [Behavior] Follow an item and it should be add to the list


def test_addItem():
    sleep(10)
    os.system('adb shell input tap 212 1454')
    sleep(5)
    os.system('adb shell input tap 110 1714')
    sleep(2)
    for i in range(2):
        os.system('adb shell input keyevent 4')
        sleep(5)
    os.system('adb shell input tap 100 100')
    sleep(5)
    os.system('adb shell input tap 529 877')
    sleep(10)
    os.system('adb shell input swipe 800 500 800 1200')  # refresh the page
    # 9. [Behavior] Navigate to the detail of item


def test_detailItem():
    sleep(10)
    os.system('adb shell input tap 427 855')
    sleep(10)
    os.system('adb shell input tap 553 138')

    # 10. [Screenshot] Disconnetion Screen


def test_Disconnect():
    sleep(5)
    # os.system('adb shell input swipe 500 5  500 1000')
    # sleep(3)
    # os.system('adb shell input tap 103 318')
    # sleep(1)
    # os.system('adb shell input tap 828 335')
    # sleep(1)
    # os.system('adb shell input tap 634 1753')
    os.system('adb shell svc wifi disable')
    os.system('adb shell svc data disable')
    sleep(2)
    os.system(
        'adb shell screencap /sdcard/screen.png && adb pull /sdcard/screen.png disconnect.png')
